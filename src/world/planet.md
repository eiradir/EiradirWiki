## Planet Eiradir

Eiradir, or "God-Stone" in ancient, is a living planet mostly consisting of water. Majority of its vast surface is ocean, a dangerous, wild, and much untravelled territory. None who left to explore the open waters had returned to talk about it. Not even rumours survive such travel. Few paths have been treaded by the world's inhabitants and thus charted on the maps. These safe paths appear to be more shallow waters and presently make up the known world.

![Planet Eiradir](./img/planet.png)

Inside the planet is a solid core made of God-Stone, substance not found anywhere else in the world. Within the core, there is a liquid inner core, thick and ever shifting, thus causing an uneven cycle of rotation to the planet, and determining its eventual shaping. The nature of this core is correlated to the planet's ability to orbit Solair in the fashion it does, and possibly make the entire system function without planet collision. An unanswered question in this subject is the mysterious role of Tanaus, a dark world on the other side of the sun, which balances Eiradir's pull. The planet's population is largely ignorant of the structure and workings of their world, and only the most educated hold the privilege of such enlightenment.

Eiradir is a host to many sentient races. The most prominent of them are the adaptable humans, the clever elves, industrious dwarves, the fierce and mighty orcs, and the savage silve. Others, less common, survive deeper within the planet's hard to reach corners.

## The Four Islands

With large populations comes great responsibility. And with so little dry land to live on... comes little hope for peace, and a large seed for conflict. After the fall of Hadenheim, a huge continent inhabited entirely by humans, The Four Islands became the only known land to most. Three of these islands were divided between humans, dwarves, and elves, and called respectively Hemara, Darkon, and Caeril. The fourth, Big Island, proved a lot harder to establish a foot on, and it remains a land of equal opportunity to all, as well as a source of great danger, as it is torn by a dire war between two brutal and powerful: the orcs and the silve.

![The Four Islands](./img/worldmap.png)

Hemara, the human island, featuring a plentiful balance of fertile land, mountains, forests, and desert, is a constant political strife of its own, land being split between the native tribal Invari ("The no-names" in ancient), and the civilized settlers from Travia and Kassar, who are not the best friends between themselves either.
Hemara is no doubt a land of political opportunity and influential growth, if one chooses to stay strictly within the traditions of humankind, no matter how divided. At least you know you can always count on your enemy to not devour your corpse after they kill you.

Caeril, no doubt the most prosperous and peaceful island, is actually a set of several heavily forested islands under sovereign control of the elves, who call themselves Highborne. The elves hold a significant advantage over many races by being in harmony and agreement among themselves, and in unity over their society's structure. They are devoted to their governing systems, and those who disagree have been quickly outcast. The elves are the oldest and the most knowledgeable race when it comes to nature, academics, magic, and history, thus they keep a high opinion of themselves as a people, accepting that their unity is their strength.

Peaceful social climate is upheld by firm discipline and long lasting doctrine.

However, racial unity comes with a potent flaw. The elves' greatest weakness is their low tolerance for other races as equals. Opinions started changing among the young Highborne about relationships with other races, and for the first time in many centuries, the never melting ice of unity may finally start to thaw and crack, as the oldest elves begin to witness the blossoming of their long feared nightmare: the ever growing number of halfbreeds. Losing purity of elven heritage may finally split the perfect harmony of the caste based society, and it is up to them as a people to hold it together.

The elves possess the only known to have ever functioned device capable of observing distant skies in detail, called an astroscope. Also, among other treasures, the elves of Caeril enjoy a unique rare natural resource - a metal called Teranium. This green coloured mineral is found deep within Caeril mines and its production serves as the basis of their economy. When smelted through proper process and subjected to certain arcane rituals, Teranium alloy is a potent substance for channelling magic energy.

Darkon has seen its share of spears, arrows, and betrayal over the years, and did not have an easy comeback. An established stronghold of the dwarves for many centuries after their flight from Tredogor and a long battle for this island, Darkon boasts a rich mountainous landscape, and deep underground caverns. Darkon is a pyramid island, with majority of its volume submerged underwater. Careful design of dwarven dungeons keeps deeper levels from being flooded with water, but anyone who is not natively from here, will surely feel uneasy and pressured as they descend below the sea level.

Darkon's power base is split between two allies - Clan Hammerfist, which resides in the Malphite Mountains, direct descendant of the monarch dynasty of Tredogor, and Clan Ironclad, their surrogate non-paying vassal, which resides in the Iron Mountains to the north. Darkon offers the dwarves an ample supply of a their own rare mineral - Malphite. This metal, when smelted into an alloy, holds record for the toughest known physical resistance known to blacksmithing, and also possesses a property of blocking magic.

The dwarves of Darkon had embraced their financial savvy and opportunistic side, and opened doors to human trade. Their main export steadily remains smithed items and raw metals, and in return, they import wood and sand, of which Darkon has very little on its own.
