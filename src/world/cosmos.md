
## Solar System

It is commonly believed and scholastically noted that Eiradir is a world shaped into a form of a concave ellipsoid sphere, which rotates around a star called Solair, Eiradir's sun. The planet is mostly covered in water, cold at the poles and warm around equator, with a liquid outer core and a displaced solid inner core.
In addition, it has two moons, Noda and Vio, and a counterplanet Tanaus, whose existence was derived theoretically, orbiting Solair on the opposite side of Eiradir itself.
Solair itself is not a perfect ball either, and continuously changes alignment of its ellipsoid elongation.
As a result of these factors, Eiradir slows down its rotation halfway past and halfway before the winter and summer solstice, and speeds back up, past half way the closer it gets to the spring and autumn equinox. This causes the planet's habitable zones spend more time exposed to the sun than away from it. This in turn makes Eiradir's summer and winter seasons last longer than its spring and autumn seasons.

Nobody has ever visited on the moons, on Tanaus, or in blackness of the high sky, and lived to talk about it. Some extremely advanced mages saw it as their goal to conquer and rebuild the universe, learn secrets of space, but none who tried have returned from the glowing blue portals that they entered driven into madness.

Winters and summers differ in length from springs and autumns, attesting to the fact that Eiradir speeds up its rotation closer to equinox, and slows down as it nears soltice. Some say that this system could only be possible to be stable if a constant divine force, or forces, were keeping their pull on its balance.

![Eiradir Solar System](./img/space.png)

Eiradir and Tanaus are not the only planets around Solair. Ten dormant worlds drift in the black vastness around them, believed to be remnants of the Gods themselves.
Each month one of the dormant planets comes into visibility as a glimmering dot in the night sky. It is believed that each of those planets was an attempt by the deities to create separate worlds of their own without collaborating with others. Resulting planets turned out uninhabitable by beings like us. What may or may not dwell there is a riddle we will likely never solve. Dead worlds, each possessing critical flaws and vital misconceptions to make true living of body and spirit impossible. It is taught that Thros, for example, is a world of stone and metal, rich with minerals and beautiful gems, and yet is void of life, for The Great Smith had no vision of spirit immaterial. Or Lelis, an ocean of mist which manifests spirits and dreams filled with all of one's most daring desires, and yet no solid surface to step on, for the Goddess of Passion had no hand for things structural. Many suggestions on these dormant worlds can be found in well kept ancient tomes, but to most such knowledge if ponderous.

Legend teaches that Eiradir is the youngest and the only inhabitable planet in the system, thus dubbed God Stone by its dwellers.

## Stars in the Sky

Beyond the reach of Solair lay the myriads of stars in the never ending sky. Sages speak that the distant stars are home to where the Gods retreated after our creation, where they rest in their abodes. Star constellations thus were detected, mapped, and named after the Gods, though most of this ritual teaching was made popular by the Highborne, and thus many stars carry Highborne names written in elven tongue. Study of stars had opened a whole separate scholastic discipline, which eventually birthed a popularly accepted set of scrolls that define the star layout. These constellations became symbols of the deities for common people.

![Eiradir Star Signs](./img/stars.png)

Unlike the dormant planets in Solair system, the stars are always visible, though sometimes a number of them seems to be less visible or disappear entirely from time to time.

The brightest star in the sky, North Star, belongs to Heraphis constellation, attributed to the deity Heraphis, patron of seafarers, travellers, and merchants. It is said that the tip of the feather is a guiding beacon which keeps all navigators oriented properly. Seeing North Star unobstructed and clear is a blessing and a relief for every ship captain.

Similarly, it is believed that seeing certain stars of the Lelei constellation blinking on a wedding day is a good omen of passionate and fertile union. The Kiss star prophecies eternal and heated love, while The Hearth promises fertile and plentiful conception of offspring. On the other hand, seeing Deidre and Nymus more prominent upon a wedding day spells drama, betrayal, and misfortune, in accordance with the myths of Nymus who fell to the Goddess's feet.

It is believed that seeing the Seven Eyes stars in the month of Kos is a bad omen, especially if the red star is seen clear and shimmering, blinking, it is said that The Dread Lord thus faces his prey and studies their souls in search of ways to corrupt them. Most people keep their gaze down away from the sky during that dark month.

The myths, truthsayings, prophecies, and superstitions about the night sky are as many as there are villages on Eiradir. One may opt to believe them or pay them no mind. Yet the stars... the stars are still there, one way or the other.

## The Hidden Threat

![The Hidden Threat](./img/threat.png)
