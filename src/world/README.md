# The World of Eiradir

The legend is but the best of what exists for the long standing principles of our understanding of the world.
Very little is known of the universe and its cosmos, for very few have survived who had the luxury of being taught or otherwise witnessing the secret truths. The only known astroscope that had ever worked is kept under high protection by the ancient elf wizards of Caeril, and it is from them that most of the knowledge about astronomy and astrology comes out into the realms. But as this wonderous device was damaged during the war, even that knowledge is at best just that: legend.

Some things, however, are close enough to us to see from time to time.

## Calendar

[![Thros](./calendar/img/month_thros.png)](./calendar/thros.md)
[![Calas](./calendar/img/month_calas.png)](./calendar/calas.md)
[![Sakos](./calendar/img/month_sakos.png)](./calendar/sakos.md)
[![Prias](./calendar/img/month_prias.png)](./calendar/prias.md)
[![Lelis](./calendar/img/month_lelis.png)](./calendar/lelis.md)
[![Herus](./calendar/img/month_herus.png)](./calendar/herus.md)
[![Voros](./calendar/img/month_voros.png)](./calendar/voros.md)
[![Nephus](./calendar/img/month_nephus.png)](./calendar/nephus.md)
[![Mes](./calendar/img/month_mes.png)](./calendar/mes.md)
[![Kos](./calendar/img/month_kos.png)](./calendar/kos.md)
