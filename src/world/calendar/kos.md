# Kos

Beware, beware, beware of the blightful eyes. Tread with care, and act with caution. Speak and think no wickedness, for at this time, wickedness will take hold.
In the midst of winter the world's nature is deep asleep beneath the snow. When one is in a slumber, the perfect time comes for dreams, and among them, nightmares. The Moon of the Eye, also known as the Blood Moon. The Seven Eyes open in the sky, as no mortal knows rest at night.

A dark and tormenting time for nearly everything pure on the planet, as Eiradir falls for a short period of time under the red light of the dominant planet Kos.
It is told that this crimson fiery molten stone serves as a gods constructed prison for the Dread Lord. The forces of Cosmos, or perhaps the increased pull of Kos, force Eiradir to travel faster along its winter orbital, making Kos the shortest of the months. It was attempted by many righteous scribes and priests to not even acknowledge this month among the others, and extend these missing 18 days of the year as days of Mes, but those who took initiative in making that happen had faced most dire circumstances unexpectedly. There was no reasonable denial that the planet Mes is no longer in prime, and very different forces come into play. The Lord of Terror demands his place among the calendar.

Truly dark, dangerous, and evil things tend to manifest in the month of Kos. The creatures which bred and brewed and fed on the sorrows, fears, and depressions of winter, now come out and strike full force, attempting to twist the world into one of grotesque, and spoil the winter jolly. Cruel winds and merciless snow storms ravage the lands, the ocean is in cold unrest, and the ground is frigid.

Kos marks the time of the summoning for Crimson Revenants, the wild servants of the Nightmare God, crafted into abominations from human souls who worshipped him in their lifetime, or were not strong enough to resist corruption. These revenants are known to lead armies of lesser demons and fiends and challenge the wills of the living.

- Season: Winter
- Days: 18
