# Mes

Mes is a mystical and solemn month, a time feared and met with much piety. The planet is on the dark side, facing Solair with the ocean, speeding up to pass its long winter orbital in the darkness.

In Mes, after blizzards and heavy snowfalls of Nephus, the world sleeps under the snow. A time of darkness and calm, the nature is at rest.
The Reaper of Life roams the sleeping realm among the snowy winter visage. The old and the sick are said to be taken to their rest in heavens.
Mes is infamous for taking those who await death to their final destination. In most cultures people spend this month remembering those who died and paying respect at the cemeteries.

As the inhabited world does not get a lot of sun and heat at this time, most agricultural activity stops.
On the 11th of Mes every year, the shadow counterplanet Tanaus aligns with the passing planet Mes and with the two moons as they pass each other, resulting in a total eclipse of the sun for one day, called The Passing of Shadow. It is told that Methos, Lord of Death, rides his white chariot through the world and personally collects the worthy.
Other, darker rumors claim that the Passing of Shadow is a night when old souls of the dead come back to the world of the living, revived by the Death Lord's divine power for reasons unknown. It is believed that such creatures made of Methos's darkness, called revenants, can be seen travelling on the snow covered roads alone, and knocking on doors.
Opinions are split on what is a worse omen - letting such a revenant in and offering to rest and spend the night, or rejecting them the requested help.

- Season: Winter
- Days: 30
- Holidays: Passing of Shadow (11th)
