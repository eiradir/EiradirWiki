# Voros

A month of equal day and night, The Raven takes away the last remains of summer. A shadow falls on the world, breaking away from the remainders of the sunny days and welcoming winter with its footsteps. Late autumn winds give way to regular night freezes, and rains raze the marshes.
Ample cloud cover keeps the realms in near constant shadow. A season of rains and thunderstorms, it transforms many meadows into swamps due to lengthy floods. Rivers break their boundaries and expand into their banks.

Not many people welcome the autumn equinox, but those who prefer night to day and darkness to light greet the Raven as their harbinger. Also, people who live in the desert greet Voros for bringing them rain, the greatest treasure and charity a hot clime could wish for.
Traditionally, Voros carries an air of superstition, having a notorious reputation to bring perfect weather for thieves and bandits.
Legend speaks of the shadow winged raven spirit watching over those of poor fate, and on occasion rendering them unseen.
On the other hand, in its dark moods the Raven can be quite cruel to those he disfavors for breaking their creed.

The Vorrigan constellation is in prime at this time, and very seldomly, a trained eye can spot the god-named planet Vorris, which can be seen in the sky as a shadow on the background of night's blackness, in the short moments of the sky being clear. Vorris, just like the Raven, does not like to be seen, and almost never lets the sky clear enough, if only on special occasion and usually with a meaningful reason.

During this month, caring parents prepare their children for winter survival. The first advice is to not stay out after dark, and never stray far from home.
By now, the last harvests would be over, and people would be ready with their food supplies stashed for the cold times. A rare person does not have food in storage by the time Voros comes, if they had an option to procure some. However, those of ill fate and limited ability seem to get by well enough. Perhaps, they are being watched over, for now.

- Season: Autumn
- Days: 30
- Holidays: Autumn Equinox (2nd)
