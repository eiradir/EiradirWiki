# Prias

Host to the greatest holiday of the year, Prias brings the Summer Solstice. The Moon of Priroda, also called as the Moon of the Tree, is the month of the longest day and the shortest night. Eiradir enters the full momentum of its long orbital along Solair, giving its inhabited hemisphere to the sunny side. The planet thrives, and the world is green.

Trees are in full green dresses, fruits go ripe and forests go from flower to berry. Prias is a known month for farmers to rejoice and ask for plentiful rain and rich soil. This is the time of harvest.

The common delicacy of the season is lorma, a summer fruit, which goes ripe only now and wilts if not picked by autumn. The month of healing, hard work in the field, and grand celebrations by night. This is the time of tightening bonds and strengthening relationships, a time of beginning love and a time of prayer, songs, and dancing. People join hands and gather in circles, asking the sky for rain and warmth, and they receive plenty. Engaged couples start their preparations for weddings. In several cultures Prias is believed to be good month for conception of strong healthy children.

Humans and elves alike celebrate the victory of those who fought in Sakos over evil, and come to relax and enjoy the summer heat. Those who fell in battle are given to earth with prayers of returning back to nature.

In the dwarf and orc communities Prias is less loudly distinguished, but not overlooked. The mountain dwellers pay thanks to the nature for the sturdy homes they were given.

- Season: Summer
- Days: 30
- Holidays: Summer Solstice Festival (15th)
