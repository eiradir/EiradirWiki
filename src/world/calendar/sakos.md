# Sakos

The first days of summer are marked by the Moon of the Hind. The trees begin to bear fruit, and the forests are covered in flowers. This is the time of successful hunt and plentiful game. During this month, some hunters claim seeing a great white hind in the forest, who leads them to a worthy and challenging beast to hunt. The same hind has been said to let lost children in the woods follow it back to their villages, and lead wounded warriors to a healer's hut. The Hind is said to be an animal spirit of Sakira, the all renown matron of vigor and courage.

This month, those who follow honor and piety, are believed to receive blessings, it is a time of tournaments, duels of justice, and trials of combat; a time of challenges. Many martial schools choose this month to be a day of exams and graduations for young men and women learning to be warriors.

While a hunter awaits Sakos with excitement, and a student prepares in anticipation for their test, a true adventuring warrior braces for action, because there is plenty to come. Craftsmen, who had worked hard all spring to refill their stock, bring out their best weapons and armor, and other battle implements, because followers of Sakira and their allies, often choose this month to mount crusades against the servants of darkness, and regain all ground lost in winter. It surely is a time of battle, often bloody, in hopes to maintain the upper hand in the conflict with forces of evil.

Also, dangerous beasts, awoken by the wondrous magic of the previous month Calas, grow to full power and roam the roads, inviting brave warriors to face them. Not always evil, these beings are strange and bizarre, so mistreating them can be dangerous.

One who lives by the sword and flame has plenty of work in Sakos.

- Season: Summer
- Days: 30
- Holidays: Summer Greetings (1st)
