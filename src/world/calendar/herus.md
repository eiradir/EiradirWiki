# Herus

The North Star lights up in the sky as an omen to all who seek movement and journeys.

Herus brings autumn on its wings of wind. Eiradir slows down after passing the long summer orbital, and enters a colder segment of rotation. The Moon of the Feather is called such for its windage. The first half of the month brings about warm winds, that grow colder with the second half. Wind sheds the trees and the temperature drop wilts the greenery, bringing the nature and its people closer to the autumn equinox.

Culturally this month is considered a time to travel far to seek a warmer clime, trade, or adventure. Seafarers greet Herus with cheer, as the winds help ships cover their routes faster. The North Star is prominent in the sky, aiding navigators in finding their way. Clear skies are considered a blessing of the autumn deity. Some sailors, after having been at sea for many months, tell tales of seeing a figure faintly appear made of wind, showing them the way, when the stars are obscured by clouds.

Heraphis does have darker moods, sometimes displeased, he conjures storms upon the sea. These are largely feared by all of naval professions, and thus, on the first day of Herus, festivals and sacrifices are organized to honor the wind god. If Heraphis is pleased, the month brings profitable ventures and money in plenty to all grateful merchants. They hurry to the safety of their homes before the next month comes, for where there is honest profit, there will always be dishonesty following suit. Fleets and caravans returning to their homes loaded with money attract attention of thieves and pirates, who find their luck in the month of Voros, so it is sufficient to say that autumn is the time of mortal affairs. Mercenaries and bodyguards are in high demand for protection during this time. But while the North Star shines, the night is not yet so dark. Days are still longer than nights.

- Season: Autumn
- Days: 30
