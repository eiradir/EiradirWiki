# Calas

Once the world has melted down from the icy embrace of winter thanks to the Great Anvil, the warm winds and sunlight are brought by the Gray Owl. The world, awoken from its slumber again, fills with wonderous things, as the nature comes back to full life and fills with magic.

It is a time of mysticism and miracles, and unexpected resurrections. The owl brings wisdom and knowledge to all who seek it.

The month of Calas is the month of the Spring Equinox, when the planet enters a balance that lasts only one day, between its long orbital and its short orbital, resulting in equal day and night length. This holiday is a big event for most cultures, a time of merrymaking that marks the month's opening, the middle of the season and celebrates life. Different cultures celebrate it in different ways.

- Season: Spring
- Days: 30
- Holidays: Spring Equinox (3rd)
