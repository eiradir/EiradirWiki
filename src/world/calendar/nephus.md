# Nephus

Nepharis, a distant planet of volcanoes and ash typhoons surrounded by a ring of dust travels across Eiradir's path, coming into view against Solair, and the constellation of Serpentine Adder comes to its prime. The Serpent of Chaos is feared by most, and the winter it brings will give little break from fears and worries.
Once a benevolent power, now the Adder represents the opening stage for the season of darkness.
The two faced deity, the goddess-god of chaos and chance, Nephar - rules this month. Some devout scholars believe that she or he, was originally female, and thus refer to Nephar as a she. Regardless of the gender or beliefs of the mortals about her, Nephar begins her rule with merciless hurricanes and blizzards.
Heavy snowfalls make it harder for people to lead their daily lives, the roads get covered and trade routes blocked off.

Chaos element itself comes on snowy wings of winter. Each time as unique as a snowflake itself, a touch of randomness in every design. Creatures who were dormant, come out in the heavy gale and claim their domains. Mountain trolls crawl out of their caves, encouraged by the lack of sun. The Adder of Nephar, her serpentine avatar is said to be wise, but also as insane and bestial as the deity itself. Unpredictable like any other snake, it may coil around you and lead you to water, or strangle your neck and give you its poisonous bite. It is wise to be fearful of an entity that makes decisions based on chance and current mood.

Those foolish or brave enough to be alone on the road in these first windy days shall face unexpected discoveries or unpredictable hardship, and in either case be subject to the will of fate.

Few and far apart, priests of Chaos greet this time. Prophetic visions visit them and previous prophecies are fulfilled. Sometimes, however, they only make sense to the ones who receive them.

Most cultures at this time get ready to face the challenge that winter will bring. A smart person this time is the lucky gambler, who stays indoors and bets his life on a flip of a coin at a tavern table. The dull man is the unlucky one who loses it.

Not all is gloomy in Nephus. The days are short, but they are still there. During daytime there is always room for winter fun. Children always stay children and enjoy their play outside. For childhood, chaos also means pleasant surprises and wondrous tricks. First snow is always a treat for those who live in snowy areas, and snowball fights can be seen in the neighborhoods. Sled hills, snow figures, icicle eating contests that result in mothers lamenting over their children's sore throats - all of these things serve a reminder to the people of the Four, that in spite of tough times, life is still life. Hope is in the living itself.

- Season: Winter
- Days: 30
