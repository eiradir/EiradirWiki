# Humankind, The Arrivals

Humans are a widespread race, predominant on Hemara island. Their domains are urban cities of stone, wood, brick, and straw. They have no advantages and no disadvantages, they are highly adaptable to any environment, and multiply quickly, which puts them at odds with the other races who have occupied the realm before them. Human traits are fairly unpronounced, with slightly favored **dexterity** and **constitution**, making them better than average craftsmen and survivors.

## Human History

The oldest dating records of human history are attributed to Virmer, The Scribe, a famous librarian and archivist, authoring numerous chronological editions and almanachs, narrating ancient history and describing humanity's journey from Hadenheim and events surrounding the continent's death. His works are many, but it is difficult to pinpoint when he was alive. His works date from 2nd to 50th years of the post flood century, but describe events as witnessed first hand, that happened centuries before the flood. This suggests that books found now are revised editions, suggesting also that Virmer could have been a collective image of several people living at different times and using the same pen name. Neither of these facts can be denied or confirmed, as Virmer remains a mystery figure.

However, the historical accounts of Virmer are correct. Human legacy lays far overseas. Over a century ago, humankind inhabited a massive continent named Hadenheim, full of abundant vegetation and fertile soil. They have established numerous kingdoms, the most prominent of which were Travia, Heimaborg, Lysandria, Radomir, and Shudda. Aside from these populated kingdoms, a number of spread out tribes and independent orders existed as well, the largest of which was Kassar, a militaristic northern regime of mountain dwelling hunters and blacksmiths.

Due to circumstances unknown for certain to this day, caused by either men themselves or by cataclysmic forces from outside, the tectonic base under Hadenheim's solid surface began to collapse and its rock plates began to shift. Earthquakes shaking the land consistently, the continent began to sink.

Desperate for survival, human kings and warlords had to escape from the land they thought would never fail them. As the planet's dominating massive water surface was largely unexplored, the rulers had no determined plan, and decided to send their fleets to opposing locations, in hopes to find more habitable land, and prayed to the gods for success. The Warrior King of Travia was the last one to accept the end of the homeland. When no other kingdom was left in the lands, he sent parties of clerics, druids, and paladins to try and calm the earth, so that he could expand borders of Travia onto the now abandoned kingdoms. But as fate would bring it, the Warrior King had to learn his defeat, as the floods and the quakes had reached the walls of the Travia Capital itself, and that was the end of the efforts to keep footing. The King looked over his ruined dominion, and realized the irony. At last, all of Hadenheim was his, and it was completely unlivable. So it had to be. Travian fleet was assembled, and the settlers sailed south.
Everyone who could leave, left. Hadenheim was no more.

Out of a dozen of fleets sent out in all direction around the planet, only two are found today as surviving nations: Kingdom of Travia, and the tribe of Kassar. They ended up on the same island, which was named Hemara, in honor of Captain Hemara, whose ship reached the banks first. The lucky first ship belonged to the Kassarean regime. In this manner, Kassar was the first culture to have arrived on Hemara. The ships arrived to the shores late at night, and people rejoiced, having long awaited to unboard. Everyone felt the longing to see the new land, and cheers were heard in the dark beneath the moons.
Following their trained instinct, they headed into the mountains, where they hoped to find their most dearly cherished resource, iron. Prospecting the mines proved extremely fruitful and opened the door to new establishment of the Kassarean civilization. The mines were rich with iron, copper, silver, and gold, and with many precious gems. Very soon, Kassarean steel has returned to production, in even better quality than ever before. Once the mountain fortress was completed and the township was established, Kassari turned their attention to populating the rest of Hemara and getting hold of trades they have not advanced in before, such as agriculture and woodwork. But just before they got down from the mountains, another fleet had arrived to the shores, flying a kingdom crest.

The second fleet to arrive was the Travian fleet. Virmer saw it as an irony. Kassari entered dispute with Travians before, but they knew not to underestimate them. Travians unloaded with elite infantry troops two thousand strong, and situated on the fertile farmland in the western side of the island. The first thing they built were the stone walls of their new kingdom. Kassari decided to negotiate first before taking action. Vincent Travian, the Travian King felt somewhat offended that Kassari claimed dominant rights to the island, since they arrived first, and it was a considerable failure to the state of colonization. So he retorted with philosophy, stating that he was the last to leave the continent, when everybody else ran from it, thus proving his noble intent and patriotic leadership. Travians supported their king fully in spirit, and proclaim him the monarch of all western Hemara, up to the Kassarean mountains, believing him to be a good king.

Once Travians got off their ships, they developed fast. The city was erected in a matter of five years, and the outskirts were populated with farmland and mills.

As the settlers went further into the island, they were surprised to find other, smaller human tribes that were assumed to be native to Hemara. Meanwhile, three more islands were discovered, and then humans realized they were not alone. To the east laid Darkon, populated by dwarves, and south of them was distant Caeril, island of the elves. The majority of attention was given to the Big Island in the south, which became the next colonization goal. Little did they know that in the heart of Big Island, dwelled a powerful civilization of its own.
Humans in general are industrious and are excellent survivors, due to their compatibility and ability to reproduce with anything humanoid and multiply. Other races view them as a fast expanding civilization of invaders, which poses a threat to both territorial stability, and pure blood. An increasing number of human halfbreeds troubles the elders. Today humans are spread amongst several hot spots within the Four Islands.

If you are a human, prepare to face the fact that there are creatures out there more powerful than you. However, your wit, social skills, and adaptability have carried your predecessors far in the world, and you hold immense potential. Whether you are a righteous Travian paladin, a bold and daring Kassari loyalist, a ruthless Invari raider, or a sneaky Sadakari pirate, or perhaps a lone wanderer looking for your own place in life, remember that you hold an unforgettable edge over others: your short lifespan, which allows you to experience life to the fullest and appreciate its value.

## Human Cultures

### Travians

A large western culture, originating in Hemara. Urban, economically prosperous, culturally advanced. Capital city: Travia. Travians occupy majority of farmland and grasslands of Hemara, and began to lay strong claim on the Big Island. Like all large societies, Travian political society is unstable, and often prone to civil war between supporters of the feudal regime lead by a line of monarchs and supporters of a democratic republic.

- Society: Feudal Dictatorship / Representative Democracy
- Trades and Arts: Farming, Fishing, Foraging, Hunt, War, Carpentry, Smithing, Jewelry, Masonry, Construction, Music, Dancing, Creative Arts, Leatherwork, Pottery, Culinary Arts
- Military Structure: Mandatory Army Service
- Popular Deities: all
- Key Values: Stability of State, Moral Excellence, Family, Tradition, Wealth, Peace

### Kassari

Northerners, mountain folk, a tough, short spoken and stoic people. Some see them as crude or barbaric in their ways, but the Kassar hold a firm niche in the cultural layout of humankind. They are religious, educated, and merely hardened by the harsh survival conditions in the cold mountains. They are famous for production of Kassarean steel, and many of their clans are home to finest human blacksmiths.

- Society: Clan Council, Caste System
Trades and Arts: Blacksmithing, Jewelry, Carpentry, Mining, Masonry, War
- Military Structure: Warrior Caste
- Popular Deities: Sakira, Throdin
- Key Values: Strength, Courage, Resourcefulness, Independence

### Sadakari

Indigenous population of a small southern island Jov'Sadakar. Primarily a community of fishermen, bandits, bounty hunters, and pirates.

- Society: Meritocracy
- Trades and Arts: Fishing, Foraging, Hunt, Mercenary, Piracy, Martial Arts, Navy
- Military Structure: Brigand Groups, Mercenary Companies
- Popular Deities: Sakira, Vorrigan, Heraphis
- Key Values: Survival, Freedom, Good Business, Good Fortune

### Invari

Easterners, the desert folk, inhabitants of the sands. Tan, dark haired, dark eyed nomadic people. Rich history of body art and many varieties of clan tattoos.
They migrated from Hemara after the Kassar‐Invari war, and are spreading across Big Island deserts. Possessing desert styles of combat, Invari are known for deadly assassins.

- Society: Tribal
- Trades and Arts: Mysticism, Herbal Remedies, Alchemy, Glassblowing, Pottery, Dancing, Blade-Dancing, Theft
- Military Structure: Assassin Clans
- Popular Deities: Heraphis, Vorrigan, Lelei
- Key Values: Ancestry Worship, Love of the Land, Blood Vengeance, Life of Pleasure

### Orilians

Secluded and secretive population of druids, living in mountains and forests, island Orilis. Few disturb them, and rightly so.

- Society: Cult
- Trades and Arts: Herbal Remedies, Druidic Lore
- Military Structure: Unknown
- Popular Deities: Priroda, Calypso, Methos
- Key Values: Preservation of Natural Balance, Gifts of Nature, Humility, Serenity, Study and Purity of Spirit
