# Silve, The Savages

The silve are the most fierce and powerful combatants. Beasts by nature and killers by design, silve push the definition of 'savage' to the extreme, and in this they rival and surpass even the orcs.

A typical silve is built as an upright standing half humanoid, half feline, largely reminiscent of a biped tiger, covered in thick fur and equipped with sharp claws and a mouth full of fangs. A point of note, the silve come in several pelt colors and stripe or spot patterns, but do not have a direct natural relation to tigers or leopards. Their pelt pattern and their facial structure are uniquely different from the animals they resemble.

The silve are large, strong, fast, and short tempered. They are excellent night hunters, with high strength and agility, and pronounced constitution. Abundance of physical attributes is compensated by lacking of mental attributes.

Socially, the silve have many enemies, and have lived in slavery of the Orc Horde for over a hundred years. Playing a silve almost always results in playing a member of a minority, oppressed, feared, and disrespected.

These creatures got the short end of the stick, and start out in a less beneficial mode. Creation of a silve character allows you to start either as an outcast living in a small desert settlement, or a slave within the Orc Horde.

This condition may be subject to change if the game's political dynamic drastically changes. We cannot stress enough that playing a silve is essentially "hard mode" of Eiradir, for you start out with less freedom of options and harsher conditions. It is a challenge not many new players would wish to take, and is recommended for more mature roleplayers.

## Silve History

These desert creatures have evolved a long time ago, with not enough hints at their divine heritage. They have always been a tribal race, worshipping the elements of nature as well as the deities.

The silve are the most instinct driven of all sentient beings. They care strongly for their young, for preservation of species, and for their mates. On the other hand, they face issues with reconciliation when it comes to two silve fighting over an argument. Once blood was shed, making peace is difficult.

Silve are proud, valuing strength and prowess over all, as a merit of right. When making decisions and arguments, it is not always the reasonable sound opinion that wins, it is the opinion of the strongest. This property of silve culture had led to its tragic history.

The silve became one of the first truly difficult obstacles in the way of the Orcish Horde, which came out of the mountains and spread out the surface.

The two races met before the orcs were organized and unified, they met in the desert, for the silve have previously lived in the deepest parts of the forest to the west, but when they started venturing further away, they got noticed by the orcs. The forest race and the mountain race have never met each other before, until their meeting in the desert. Nobody knows how the first clash occurred, but after some time, it was established by each side, that the other is a menace.

After this point, numerous clashes took place, mostly because the orcs would send scouting squads inside the forests. Not many of these scouting squads made it back, because of the silve knew the forests too well and employed instincts of silent predators. They struck and vanished in the jungle. The most gruesome battles, however, always took place in the desert, which was contested ground.

More and more often the silve settlements suffered raids and ambushes, which became more organized. The silve fought back vigorously against raiders, and struck back at night, spying out the orc camps and settlements as well. The small scale conflict was at an even stand still, with plenty of bloodshed on each side.

The fateful event that forced the situation to drastically change for the first time, was ascension of Rragnak The First as a Chief of all orcs, and the unified formation of The Horde. This social overhaul had rebuilt the orc culture and their structure, and soon, silve had to face not simply raider groups, which they could manage, but united cohorts, equipped with well made weaponry and armor, and under tactical cover of archers. The orcs now followed military authority and war strategy, a concept silve only understood rudimentary on small scale level, and every new battle was more and more brutal for the silve and less and less costly for the orcs. The orcs began to gain.

The silve tribes came together to discuss the growing danger.

Some, more sensible silve suggested a unification of their own, and promotion of pack leaders, to match the growing organization of orcs. These silve believed that the reason for orc success is appointment of generals, who oversee movement and actions of entire cohorts, not individual tribes. They preached division of clans and tribes into cohorts, promoting every silve to work together and become brothers.

An equal third of the silve believed that tribes and clans are just too ancient and too important to ignore the differences. They claimed that no matter how hard pressed, silve must remain silve, or their blood will be impure. They would not bend to the will of others.

A third group of silve simply believed themselves to be the best and superior, and agreed to a unification only on their own terms, with their clans in the lead. They generally stated that it matters not how pure blood of the silve is at all. Blood or no blood, all silve will be dead, unless they follow the strongest tribe.

This quickly lead to a series of duels to the death to decide which tribe of silve was strongest. As a result, most of the strongest silve leaders have killed each other off. Then, unable to let go of blood vengeance, their family members fought as follow up champions to avenge the fallen family members, and the next strongest challenger would fight. This orgy of combat and death lasted for several weeks, and was called The Blood Cleansing, or, by others, non-silve thinkers, The Silve Downfall. And downfall it was indeed, for the pile of dead silve bodies grew and kept getting higher, with all the best silve warriors laying to waste in the sand. A power that could have been used to command the people against orcs, was torn apart by itself.

In the end, only the less aggressive combatants and more sensible thinkers were left, and the fighting was over. A leader, the last fighting silve standing was in fact determined, an offspring of an offspring, a young and hot headed silve adolescent of good quickness and strength. His name was Harko, a silve with fur red as the darkest dusk. He was not as powerful or wise as his grandfather, who was the first of his tribe to be killed in the trial, but he was the best specimen available from the ones still surviving. He turned silve attention to the fact that the place of summit was now more reminiscent of burial grounds, and that unless something is changed in silve ways, the race will ruin itself.

Harko assumed command of the remaining tribe force, and grudgingly, unified them, on temporary terms. It was far from a harmonic peace, there was lots of theft, fighting, and slaughter in the ranks, but it was the most he could hope for. At least, the silve were marching together, as one. Harko took the silve people back to the forest, to build a fortified home which would serve as a keep against orcs. His main oversight, however, was the fact that the only thing around them to build with was wood.

Meanwhile, the orc civilization has advanced past commanding cohorts, and had successfully trained and raised a whole and complete legion. Supply and communication chains, support of the shamans, healer units in the back, all knew their roles and knew their timing. In this time, they have constructed a citadel of stone and iron, in the outer face of the mountain, resisting the desert and towering over it, a symbol of power and brotherhood. Rragnak was ready to end the silve once and for all. The Horde picked up silve track and crossed the desert to advance on the forest.

Needless to say, the orcs used what they knew would work best to smoke a wild animal out of its forest comfort. Fire.

Harko had no choice but to gather the silve and run, face first into the orc army. The keep, halfway finished, was ablaze, and there was nowhere to retreat. Vastly underestimating the pure strength and wild desperation of the cornered silve, the orcs had a lot of casualties, but Rragnak was relentless and unyielding. Orders were to slay all, and the orders were followed. Rragnak and Harko met in battle, creating a decisive point. Unfortunately for the silve, it was very one sided. The fight was not even close, as Rragnak was a giant among orcs. Now, at this moment, all silve saw the error in their ways. All of the strongest, biggest, and fiercest silve leaders who could possibly stand toe to toe against the mighty Chief, now lay rotting in the desert burial grounds, having fallen to the trials, the sad attempt to keep the bloodline pure and tradition alive. They witnessed how their ambition had led them to weakness, and the sealing moment of that admittance was Harko's windpipe crushed in muscular green hands with a powerful crunch.

When Harko's body rose up high in the air, spiked on orcish spears, most of the silve surrendered, some small groups took their families and managed to hide out of sight. The few dozens, escaped from capture or death, by fleeing back into the burning forest. They became the ancestors of the new generations of silve who later formed the silve rogue groups that raid and battle the orc oppression. The opression that lasted for almost four centuries and survives till today.

Today, the word silve and the term slave are almost identical in the majority of the world. Silve women are put to work, silve children raised to serve the orc masters. The silve comprise a good half of all the lowly workforce within the Horde. They clean the halls, empty out garbage, do tasks the orcs are now too proud to do. There are several things the silve are not allowed to do, like cooking or touching weapons. However, there are all sorts of deviant exceptions, some orcs were even known to treat their silve slaves kindly, trust them, befriend them, and on rarest occasions, love them. These are extremely uncommon, but for the fairness sake, being a good slave will grant a silve safety, at least for a time of their master being alive.

Not all silve are unwilling.

The unwilling ones can be seen in the slave pits, trained and beaten by cruel orc pit bosses. The silve have no rights within the Horde, and everyday life shows evidence of how little their lives are worth.

High ranking orc warriors wear cloaks of silve pelts, and orc women wear scarves and gloves made of silve. Silve heads decorate orcish halls, and the head of the Blood Throne, in the Seat of Power, where Chief Warlord of the Horde resides today is decorated with Harko's skull, and his red and black striped paws with menacing claws making ornaments of the throne. Harko's body pelt is the Red Mantle, which the Chief wears.

The atrocities of using silve for pelts have been heard of even in human cities on Hemara, and more rarely, among the dwarves on Darkon. The only true sympathizers to the silve cause are the highborne, the elves.

The orcs coined a term "onca", a word for enslaved silve in orcish language, a derogatory term considered offensive by all free silve. Indeed, not all silve are onca. As excruciating as their defeat was four hundred years ago, rebel groups still exist, and the hope may be rising.

He came out of nowhere. Nobody knows where they were hiding all these years, or how they found their way. Black as the darkest panther, muscular and huge, quick as lightning. Few have seen him, but many heard of his deeds. Dinerrb, the Black Death, leading a clan of black silve. An inspiring leader for all free silve resistance fighters, Dinerrb is rumored to be highly educated, extremely strong, and no orc has ever survived an encounter with him. A master of terrorism, he and his group operate in secret, continuously rocking the Horde society with public statements. Severed heads of powerful and feared orc warriors, with letters signed in blood. Dinerrb works to ruin the common perception that any orc warlord is undefeatable, and his ideas of freedom and unification, continuing and expanding on Harko's ideas from the past, are starting to work. However, many silve are cautious to voice their support, because of exactly what happened to Harko, and the orcs purposely remind of his fate.

If anything is known for certain, is that the orc Chief wants to see a severed black pelt added to Harko's dawn red in the Chief's Hall.

If you are born a silve, your fate dice have given you a hard path. You may accept slavery, be it willingly in kindness or unwillingly in cruelty, but it will be a burdain to endure nonetheless, a price for safety. Or, on the other hand, you may join the hard life of free rebels, joining the Black Clan on the front lines of a rising guerilla war, or in the back, working to support the ideas of freedom. You may also end up finding a path of a refugee, fleeing from Big Island, and taking asylum among other races and their cultures. Your choices are just as many, but your life will most likely be hard. The primary choice you must make is the one within yourself: to realize a mistake of wild blood selection, or to embrace tradition and deny weakness to the end, even if it means death over pride.


## Silve Cultures

### The Enslaved

- Society: Caste
- Trades and Arts: Blacksmithing, Jewelry, Carpentry, Masonry, Mining, Fletching, Tailoring, Fishing, Hunt
- Military Structure: None
- Popular Deities: Various
- Key Values: Survival

### The Free

- Society: Tribal
- Trades and Arts: Blacksmithing, Jewelry, Carpentry, Masonry, Mining, Fletching, Tailoring, Fishing, Hunt
- Military Structure: Freelance Raider Groups
- Popular Deities: Various Common and Pagan Elements
- Key Values: Strength, Independence, Freedom
