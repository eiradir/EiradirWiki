# Dwarvenkind, The Stoneborne

Dwarves are sturdy, strong, stubborn, and durable. They are a good pick for those who enjoy crafting and close range combat. This race opens opportunity to display strong personality, determination, achievement, and a sense of humor. Dwarves spend most of their time indoors in some way or another. They are natural excellent miners and blacksmiths. High **strength** and **constitution** make them amazing defensive fighters and formidable offensive melee. If anything, they lack agility because of their heavy, stocky builds, and short legged bodies make them less maneuverable in a fight. With some exceptions, most dwarves are not very suitable for using ranged weapons, such as bows and throwing weapons due to their height. Also, naturally not being gifted towards arcane magic, dwarves tend to branch into alchemy and the magic properties of nature.

## Dwarven History

Deep in the ground loud sounds of blast furnace domes and great smithing hammers are heard, and as you near the Malphite Hall, you hear the intermixed dinging of little finesmithing hammers, and the scratching music of the carpentry saws, the quick tapping songs of mining pickaxes, and the solemn low humming of the many voices ‐ an ancient song. Here, in Darkon, unrivaled in their crafts and fine arts, live the dwarves.

Up above, the Mead Hall sings a different melody. The merrymaking and the clanking of many glasses, goblets, and mugs, and clouds of smoke from the cooking meat rise to the ceilings, and the scent of ale is in the air. In the after hours, when the hard work is done, the dwarves come here to party!

In the Iron Mountains to the north, you could hear the hunting horns, and the marching beat of axes and shields dominates the presence here, as brave Ironclad sentinels train and move out on patrol.

And far out in the Misty Swamps to the west, live the dark and frightening Outsiders, secretive and dangerous, full of clandestine knowledge and strange, eery mystery. The only music here is the bubbling of swamp tar and pit moss, and the hooting of a black owl makes the midnight song beneath the moon.

A suppressing majority of dwarves worship Throdin as they are considered his direct children, for according to legend, he had crafted them from stone. With time, however, minorities began to spread.

Dwarves have had a long history of surviving as a unified kingdom. The first Dwarven King, Trogan Hammerfist, led the dwarves through an underwater tunnel from their old home in Tredogor to the new lands, following the call of their father god Throdin. The great dwarven artefact, called The Stone Book, contains chronicles and descriptions of this jouney, and most believe it to be Throdin's attempt to evacuate his children from the place of attention during the Celestial Conflict. The journey completed, and with tunnels collapsing behind them, the dwarves found their way ahead, finally reaching the warm bowels of an island they named Darkon, which was their native name for Throdin's legendary hammer. Darkon is the north eastern member of the Four Islands, and here the dwarves established their new reign.

The task was far from easy. The island mountains proved rich in minerals and superb in structure, suitable for building and full of natural caverns, but the price it came with was paid in blood. The caverns were inhibited by dangerous monsters and sentient creatures, who had their own plans for all these riches. The most powerful of these creatures were the stone giants, jotins. But they were not the only contender, and Dwarves had to fight a plethora of unified enemies for every yard of rock. Eventually, after a painstaking period of reconnaissanse and clearing, they drove off the competitors and claimed the mountains as their own. Their first grand victory was capturing the largest mountain on the island, which they named after their king, The Hammerfist Mountain.

Within the mountain, the dwarves began building their new civilization. They established their first new city, Jotingard, which became the seat of Hammerfirst Clan, the direct descendants and relatives of King Trogan Hammerfist, who remained the ruling noble family for hundreds of years. Lesser houses more distantly related to them (such as Hammerhand, Hammershield, Goldhammer, etc.) branched out and claimed nearby territories.

As more territory was discovered, and clans grew more independant, some clans petitioned to be excused from doing service and paying tribute to the King's family and provide alliance and support in exchange for independance. This request was first granted to Clan Ironclad, the most prominent clan of dwarven warriors, bred with large size, strong muscle, and minds designed for combat. The King at that time was Trogan's grandson, Grom Hammerfist. He was a solid leader, and was wise enough to give his people freedom of life and industry, however he ruled that Ironclad will remain a surrogate vassal of the Hammerfist Kingdom, who will, in letter, remain their sovereign. With King Hammerfist's blessing, the Ironclad ventured north into the Iron Mountains, under an agreed condition, that the two clans will stay connected via tunnels, and in case of military threat, the clans will unite and be as one kingdom again under the Hammerfist sovereignty. This was made law, and proved a wise decision only three decades later that helped avoid a war.

The orc horde landed at Darkon's shores at night, and at the break of dawn, they crossed the fields and the Misty Swamps, and before them lay the great mountain range. The green swarm of three thousand strong looked just beyond the hills on the horizon, and what they saw was a thick line of black blur, reaching all the way to the northern peaks of Iron Mountains. A black line with banners flying above it here and there, and the dawn's first rays reflecting in the shields. All clans united together, stood ready to oppose them: the dwarven armies.

It was said, that on that day, King Grom Hammerfist had called in all the independant clans whom he allowed to live freely, back under his banner, and every single clan has answered the call to arms. It was said, that on this day, three figures stepped out forward from each side. Three orcs stepped out: the orc chief, and two of his clan warlords. Three dwarves stepped out: King Grom, his son Gorin, and Bralin Ironclad, head general of the Ironclad Clan. It was said that the three orcs and the three dwarves approached the middle of the battlefield. The lords raised their hands, and two warlords from each side stood behind, while the King and the Chief approached each other face to face.

None could hear if anything was spoken, but it is said, that King Grom and the Orc Chief simply stood there, for a few moments, axes in their hands, fists clenched, looking each other in the eye. Breathing, their teeth tighly closed, the yellow eyes of the orc, full of destructive menace, and the old King's sternly furrowed eyebrows, with dark eyes, staring from beneath them, full of calm, quiet fury. It is said, that not a word was uttered, as the two nation leaders stood there, studying one another. After moments have passed, the Orc Chief gave a loud whistle, and turned the horde around, back to the ships. Their fleet departed, and never again was seen on Darkon. In such manner, the war was prevented.

Many years have passed since those events, and King Grom celebrated his 300th birthday. The Clans are once more independant, and new clans arose from the ground. Not all dwarves follow the path of Throdin and the mountains. Some have heard of a clan of Outsiders, who may have mixed bloods with woodlings, gnomes, or even goblins, and prefer to live in the swamps.

These dark dwarves are said to only care about themselves and their own profit, but living in the swamp gave them access to many ingredients of dark alchemy. Powerful and secretive, these witches and alchemists can prepare many drugs and poisons, as well as remedies, but dealing with them is extremely hard, for nobody knows what they truly value.

The dwarves overall have noticed the human's alarming rate of arrival and expansion, and they are aware of the elven hostility towards them. However, unlike the elves, the dwarves see an opportunity in humans to do business. Many dwarves venture to Hemara with missions of diplomacy and trade, and adapt to the growing needs of humanity, especially for quality smithed goods. Dwarven leadership allows this, and passively waits for the conflict to either spike or resolve itself. They prefer not to make a first aggressive move and decided to take sides once an aggressor becomes obvious.

As a dwarf, you live three times longer than a human does, you physically mature at around the age of thirty five, and the longest living dwarf known died on the day of his three hundred and sixtieth birthday. You have the joy of being able to meet your family and get to know them well, drink together with your great‐great‐grandfathers, and compare beards with their grandfathers. You pay for this luxury with a slightly slower reproduction rate, not as slow as elven, but a mother carries a dwarf in her womb for two years. Your distant relatives are gnomes and woodlings, but of all three, your kind is the one that prevailed in survival, and you know no rivals in underground tunneling.
Your people are naturally durable and adaptable to living in hard labor, they like their work and their gold. You are an expert on ale, bar fights, and sure know how to take a beating. Traditions of brewing, mining, and smithing are strong in every generation, and are the pride of your kind.

## Dwarven Cultures

### The Hammerfist Clan

What remains of the first dwarven kingdom, a versatile mixed crafter and artisan society of hard working dwarves, located in the north eastern mountains of Darkon. Finest crafters on the Four Islands. They trade with the Ironclad and, more seldom, with Outsiders. Hammerfist consider themselves owners of the island Darkon, and consider Ironclad to be extension of themselves. Sometimes this creates tensions, as Ironclad strives for independence. The Hammerfist welcome dwarven newcomers, because they seek to enrich their blood ties. Being mostly related, they cannot breed much within the clan.

- Society: Monarchy
- Trades and Arts: Blacksmithing, Jewelry, Carpentry, Masonry, Mining, Fletching, Tailoring, Fishing, Hunt
- Military Structure: Volunteer Army Service
- Popular Deities: Throdin, Heraphis, Sakira
- Key Values: Tradition, Family, Wealth, Quality

### The Ironclad

- Society: Military Clan
- Trades and Arts: War, Blacksmithing, Fletching
- Military Structure: Mandatory Service
- Popular Deities: Throdin, Sakira
- Key Values: Strength, Independence, Reputation

### The Outsiders

- Society: Independent Cults
- Trades and Arts: Alchemy, Herbalism, Fletching, Spying, Theft, Crime
- Military Structure: Absent
- Popular Deities: Nephar, Vorrigan, Calypso, Lelei, Methos, Koshmar
- Key Values: Profit, Knowledge, Leverage
