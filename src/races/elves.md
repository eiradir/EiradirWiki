# Elvenkind, The Highborne

Elves are an elder race, densely populating islands of Caeril. They enjoy a long lifespan, allowing for longevity of learned minds and skilled bodies. They are as wise and they are vain, and they excel at using dual swords, archery, and magic. Elves consider themselves neutral overseers of the world, and guides for younger races. Elves are attuned to the mana flow, and their traits are leaning towards magic, with largely favored **intelligence** and **arcanum**, making them superb scholars and excellent natural channellers of magic.

## Elven History

From a flying eagle's height, one could see a long and slim chain of islands to the south. It starts with the sandy beaches in its northern shores, and continues south with densely forested hills covered in all manner of life and vegetation. This place is the abode of elves, called Caeril.

Elves call themselves Aen Shiddhe, which in common translates to "Highborne". Carrying the elder blood and being the first of the created races, elves believe themselves direct descendants of Calypso. They organized their society into a caste system, with each of the three dominant castes occupying a dedicated territory and being responsible for one of primary aspects.

The northernmost sandy beaches belong to Vormaegal, a caste of halfbreeds and lesser Highborne of lower ranking houses, responsible for trade and communication with other races. They are a young caste, but nevertheless respected enough to be within the ruling council.

Just behind the beaches, facing east and north stands a fortified stronghold on the island of the dreaded Trigadi. Its walls and towers are manned without break to keep watch for invaders and pirates. The Trigadi caste of noble Highborne warriors live here and train diligently in their temple. They perfect the martial arts of bladeweaving and elven longbow under guidance of Trigadi Grandmasters, and prove time and time again their fearsome reputation as protectors and watchers of Caeril. They are the first true line of defense against enemies of the Highborne, and legends of their success in the Zamisi war live on to this day.

As you delve deeper into the main island of Caeril, you shall see a tall tower with magical pyres lighting up the sky. The magical pylons keep the island protected and controlled by forces of powerful magic. This is the domain of the Ermaedril, the highest ruling caste of elder Highborne mages. They were the first to tame arcane secrets in this world, and they guard them well. Here you may find the oldest elves alive, and they have much to tell if you earn their trust.
There are elves living all over the Caeril islands who do not belong to the three ruling castes, and belong to lesser castes. They are considered citizens.

In general, the elves are extremely protective of Caeril and to a point, of the the Four Islands as a whole. They are not always as gentle and humanitarian as stories portray them to be. Elves react warily to the quickly growing human expansion, which was hard not to notice. Majority of the Highborne is divided into two camps, those who tolerate humans, and those who wish to drive them off or enslave them. Younger elves seem to depart futher from these traditional opinions and choose to befriend humans, but older elves only see this as a danger, for the fear of breeding out pure Highborn blood.

Elven history is deep, ancient, and in places, dark. For the elder race, despite their own beliefs in superiority, is subject to all things mortals are. No matter how wise, they share the seeds of pride, vanity, and envy. These trends float on the surface of elven history throughout the ages. One must browse through the dusty tomes in the secret libraries of House Ermaedril to learn of the great wars against the Zamisi, and the taming of forbidden powers, the exile of the corrupted elder mages, and history of war against the orcs. This knowledge is not open to all, and is guarded by the inner circles. Within these circles lay many mysteries.

If you are an elf, you belong to the eldest and longest standing heritage in the Four Islands. Your ancestors have existed long before anyone else. You enjoy the longest lifespan among mortal creatures, and your blood allows you to live for centuries, a gift of nature few can appreciate. Use this time wisely, learn, and develop. Prosper and gain, but do not be blinded by power and pride, for this was the downfall of many an elf. While it is true that your blood is elder, and you are a Highborne compared to all others, consider twice whether you want to remind this to the world. Whether you are a wise and solemn Ermaedril, a vigorous and proud Trigadi, or a communicable and industrious Vormaegal, or perhaps a founder of your own caste of elven citizens, remember this: nature is the caring mother of all, tradition is important to preserve, order is important to keep, innovation brings progress, and vanity is dangerous to befriend. Find your own balance between these things and choose your path. Your race has a glorious past, it is up to people like you to determine its future.

## Elven Cultures

### The Trigadi

A patriotic culture of elven warriors, devoted to protection of elven lands, possessing secrets of Bladeweaving, a self proclaimed superior technique of using two blades in a dance movement pattern, also described as martial caligraphy. Blade maneuvers are referred to as Signs, and their combinations referred to as Patterns, weaved from Signs. Students become adepts through study and experience. Each adept who wishes to attain the rank of master, must develop his own Signature ‐ a final examination performed in front of the high audience, where patterns are weaved into a dance from beginning to end, dispatching multiple targets. The Trigadi temple is located in the western island of Caeril Cluster, referred to as the Trigadi Island.

- Society: Templar Order Hierarchy
- Trades and Arts: War, Archery, Hunt, Blacksmithing, Fletching, Inscription, Tailoring
- Military Structure: Elitist Warrior Caste
- Popular Deities: Sakira, Calypso, Priroda
- Key Values: Tradition, Strength, Efficiency

### The Ermaedril

A somewhat arrogant elitist society of elven mages, residing in the larger, eastern island of Caeril Cluster, otherwise referred to simply as Caeril. Their greatest contribution to the people was constructing Tower Ermaedril and forging the "Heart of Caeril", a focal structure of magic pylons, providing collective energy to protect the island cluster from large scale invasion and arcane destruction. This structure is collectively recharged every day by the most powerful mages. The Ermaedril are probably the best arcane mages in existence, and they guard their secrets well. They were the first to encounter magic and first to master it, and... they did not share. Thus, every non elf who learned magic is proud to have done so without the help of the Ermaedril. This culture is very protective of their environment and is just as equally patriotic.

- Society: Scholastic Meritocracy
- Trades and Arts: Arcane Magic, Blacksmithing, Jewelry, Inscription
- Military Structure: Battle Mage Unit
- Popular Deities: Calypso, Priroda
- Key Values: Knowledge, Power, Arcane Skill, Tradition, Stability

### The Vormaegal

Residents of the small sandy cluster of beach islands in the north east Caeril, these elves have darker skin, and are more open to communication with the rest of the world. They are a trader society, and end up being the most common representatives of the elven culture, acting as an unintentional emissary to the non elven lands. Because of their closeness with other races, incidents of cross breeding and blood mixing are frequent.
    
- Society: Democracy
- Trades and Arts: Trading, Fishing, Foraging, Hunt, Carpentry, Jewelry, Farming, Cooking, Masonry, Construction, Navy
- Military Structure: Voluntary
- Popular Deities: Heraphis, Vorrigan, Priroda, Throdin
- Key Values: Survival, Acceptance, Progress, Freedom, Good Business, Good Fortune
