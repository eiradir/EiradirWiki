# Eiradir

Eiradir is an enforced roleplaying medieval fantasy game of politics and strife, focusing on live character interaction, with players always staying in character at all times. In this game you will experience true immersion into the persistent, open ended, living world that breathes day to day and changes with every action characters take.

You may take on a role of an ambitious conqueror, if you are good at persuasion; a powerful swordsman, if you excel at combat; a tradesman or a merchant, if you enjoy crafting, farming, and trading; or simply a person trying to do their best and survive in a world full of intrigue, betrayal, and romance. Whether you choose to be a commoner or a noble, your time in Eiradir shall be a tale to behold, as everyone has a chance to make a difference and mark their place in the world's ongoing chronicle.

![World Map](./img/worldmap.png)

It is the little things that matter, sometimes. In the last year of the third century since the fall of Hadenheim lords, one wealthy Travian farmer chose to work all day and all night instead of taking a break, and was able to produce an extra day's worth of wheat. This wheat allowed the granaries of Malvior to have a week's worth of flour, which came to use six months later to make bread for the defenders when the city was under siege by the Lysandrians. This, at first glance small fact, permitted the city to withstand the siege for one additional week, giving enough time for the reserve forces of Ser Valentine The Brave to come in from the north to interfere, causing the siege to fail, and saving Malvior, continuing its reputation as impregnable.

The little things. Few people nowadays know what little thing caused the continent of Hadenheim to collapse and be swallowed by the ocean, and few remember those days. But the history does not forget. The Gods keep a careful tally.

Behold the times of today and now! The races inhabiting The Four Islands are at a tense political stalemate. The human nations in the north east, the dwarven clans of the north west, and the elves of the south, all build plans of influence and conquest to decide the fate of The Big Island. Some subtle, others openly militant, regardless of approach, all of them, Hemarians, Darkonians, and Highborne, will have to face the controlling might of the orc Horde, which rules the heart of the island. Perhaps this situation will benefit the oppressed silve?
