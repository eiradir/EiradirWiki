# Game Rules

If you want to report a rule breaker, be sure to be as detailed as possible so that we can quickly take action.
You can use [this link](https://forum.eiradir.net/index.php?forums/report/post-thread) to get directly to the form. You will need to log into your forum account.

 [[toc]]
 
## Account

Each player is only allowed one game account. Accounts and the underlying characters may not be given away, shared or sold.
As the owner of your account you take full responsibility of everything that happens with it, so do not give your credentials away and make sure to choose a password that is secure enough for people not to guess it.

The forum allows you to register multiple accounts using the same email. This means you are allowed to create an own forum account for each character or guild you might have, if you'd like.

## Character Names

Character names have to fit the medieval fantasy theme of Eiradir.
Therefore, the following kind of names are not allowed:

- Vulgar, offensive and overall nuisance names
- Historical or mythological names that appear in real life or Eiradir's background lore (Jesus Christ, Alexander the Great, Koshmar)
- Well known names from literature and TV (Gandalf, Harry Potter, Eddard Stark)

The name should also not contain a title such as "Sir" or "Lord".

## Character Concept

Characters must fit into the described game world within the race guidelines.
The races players are allowed to play are limited within the engine, however, it is allowed to roleplay mixed breeds.

As the game engine only supports a limited amount of races, if a player wishes to roleplay a halfbreed, the engine will not support any special properties a halfbreed may have. A player will have to choose which blood is dominant in the character, and create them with that actual race.

Any character features that are not normal to a human being (metal teeth, fire arms, night vision, retractable claws, etc.), or that require magical or divine intervention, or may potentially grant a character an edge over others, must be approved by a Gamemaster.
Even if you may be allowed to give these features to your character, they will not necessarily be supported by the client, and there will probably be no actual bonus or malus applied.

## Language

Players in Eiradir should speak as their character in the game's setting would. Use of "old english" (i.e. where art thou) is not necessary (and might even be counterproductive), however, proper english grammar and spelling should be used, and no modern references or gamer "leet speak" should be used. A player must react and talk in-character only from the perspective of the person they play (i.e. Your characters do not know what a computer is, nor do they "pwn n00bs").

Continuous use of capital letters or punctuation marks that might disturb the atmosphere is not allowed. Out of Character messages should be kept to a minimum, should not disturb other people's roleplay and should be marked by double brackets, e.g. "(( sorry, brb ))".

## Emotes

Any action a character performs should be described by emotes. Emotes are messages starting with #me and describe what your character is doing, rather than what they're saying. Since other characters can't read your character's mind, emotes should be written in a way that they only describe visible actions, but not feelings, thoughts or opinions.

Emotes that determine a behaviour or an effect on other characters and leave no room for reaction are forbidden.
"John pushes Mike and watches him fall down the stairs"
This is a so called "forced emote", because it gives the player of Mike no room to respond to the push.

## Ingame Realism

You are not allowed to abuse the client's mechanics to gain an advantage over other players - for example you may not log out in order to escape a dangerous situation for your character. As a rule, it is safe to log out of the game when nothing threatens your character for five minutes. That is considered being "in the clear".

NPCs must be treated as real beings, able to think, speak, and process important information. It is not allowed to ignore NPCs in your actions - even though there might not be any specific behaviour scripted by the engine, they still count as a witness to a robbery or assault and their reactions shall be included in your roleplay in a logical way.

While there are no permanent effects caused by being knocked out (apart from the default death penalty) and characters can only actually die permanently if the player wishes so, that does not mean characters are free from fear of death. No one would just run straight into the jaw of a dragon or repeatedly jump off a library roof without having the safety of their life in mind. If your character was knocked out, injuries should be played out for at least until the penalty effect is over and the character is at full health again.

## Out Of Character (OOC)

Everything a character does should be driven and motivated by proper roleplay reasons internal to the character, not based on real life likes and dislikes of the player in control.
The character should not share knowledge with the player which the character did not directly receive ingame. Thus, doing things like calling for help OOC on a messenger program, or attacking a character based solely on player dislike, while not having reasons to do so in character, are forbidden.
You have to be able to provide an ingame reason for your character's actions.

## Mixing Characters

It is not allowed to exchange items or knowledge between two characters of the same account.
You are only allowed log in with one character of an account at once and may not use a middleman for that matter either.

You should also make an effort to avoid connections between your characters - for example, playing the town guard's officer on one character, while being leader of the infamous thieves' guild results in a conflict of interest and has high potential of leading to situations where both characters would be expected to be online at once.

## Player Killing

Repeatedly killing a character, especially right after their resurrection, is not allowed. As a rule of thumb, each wrongful act is only worth one kill - find a creative roleplay solution if someone goes on your nerves, or hire an assassin to take care of it!

Generally, killing of players should have a reason, such as an ingame conflict, rather than just being a means of enjoyment for yourself. Do not bring Out-Of-Character frustrations into the game, and remember that you're playing a game with other people who are also trying to have a good time. Try to be creative when playing a villain, because the kind of villain that just kills people left and right isn't fun for the other people playing with you.

## Community Behavior

Harassing, insulting or threatening a player out of character is not allowed. This extends to continuous harassment of a player's characters over out-of-game reasons or trivial ingame reasons. If asked to cease interaction, try to respect that wish if ingame circumstances allow.

Hate speech is obviously not tolerated. As for hateful statements ingame, it is important to separate between fantasy and reality. For example, quarrels and dislikes between races who lore-wise do not get along well (e.g. Silve and Orcs) are fine, but using slurs or hateful statements towards characters on the basis of their skin color, sexual orientation etc. is not.

Rule breakers should be reported to the GM team immediately - do not blame and flame, do not act as the local vigilante.

Some players wish to stay anonymous and that wish is to be respected.
Generally, do not reveal a character's player identity unless you can be sure that player is alright with it.

## Violence and X‐rated Actions

Portrayal of violence and vulgar language is fine, as long as the violence is not overly graphic. Portrayal of X-rated actions, sexual assault, self harm and suicide is not allowed, however, mere mention of them is fine. This also includes consistent sexual harassment between ingame characters - while occasional innuendo and flirtation is fine, roleplay is best enjoyed when everyone can be comfortable.

To clarify the above, these are examples for things that are not okay, even if the other player has given consent:

- Cutting off someone's leg piece by piece and feeding it to them
- Implying rape or sexual assault of another character, even if it isn't actively played out
- Playing a character whose personality is based around staring at people's tits and making sexual comments

However, talking about how these things have happened in the past or exist as a thing in the world of Eiradir is alright.

## Maturity Level

The minimum recommended age for playing Eiradir is 16 years old. Your character may be exposed to serious topics and other players will expect you to act appropriately at all times.

Note that in order to make an account, you must be at least 13 years old or older. This goes for the game, the forum as well as the Discord server.

## Gamemasters

Gamemasters act as helpers, event and quest organizers, and rule enforcers in the game. Therefore, all requests and complaints about ingame matters should be directed to them, preferably using the report form , or on Discord if it's something minor.

Impersonating a Gamemaster or claiming to be a staff member when you're not is forbidden.

## Away from Keyboard (AFK)

We suggest you to always log out if you have to leave the keyboard for more than a minute.
Not only is the world of Eiradir dangerous and your character might easily get into trouble if he just stands around unattended, it also disturbs other players' roleplay if you as a player can not react to it actively.

Players found AFK for several minutes might be kicked automatically or through the hands of a gamemaster, however, this will not be noted negatively on your account as long as it does not occur frequently.

If you won't be away for a noticeable long time and don't want to relog for that matter, you should notify the players around you that you'll be right back (out of character).

## Bug Abuse and Cheating

Intentional use of game errors (bugs), for example in order to gain an advantage over other players, is forbidden. All errors found ingame, must be reported either in the forum or in the case of a possibly game-breaking bug directly to a developer.

Using additional programs or tools that are not provided by the Eiradir Team itself in order to fake a connection to the server, perform certain actions (i.e. bots) or harm the server in any way is not allowed and will result in you being removed from the game. 
