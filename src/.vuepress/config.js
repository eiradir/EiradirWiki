module.exports = {
    title: "Eiradir Wiki",
    description: "Collecting lore and basic information on common items as an entrypoint for new players.",
    dest: "dist",
    themeConfig: {
        logo: "/favicon.png",
        nav: [
            {text: 'Website', link: 'https://eiradir.net'},
            {text: 'Forum', link: 'https://forum.eiradir.net'}
        ],
        sidebar: [
            ["/", "Home"],
            {title: "The Vision", path: "/vision"},
            {title: "Game Rules", path: "/rules"},
            {title: "Races", path: "/races/", children: ["/races/humans", "/races/elves", "/races/dwarves", "/races/orcs", "/races/silve"]},
            {title: "Religion", path: "/religion/"},
            {title: "The World", path: "/world/"},
            {title: "Professions", path: "/professions/"},
        ],
        nextLinks: false,
        prevLinks: false
    }
}
