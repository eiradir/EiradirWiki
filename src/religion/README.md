# Religion in Eiradir

Cultures of Eiradir hold respect for a pantheon of ten deities. Their existence is denied by some, and is taken for a fact by others. Legends are all that remains. According to these legends, The Gods existed before all, and lived in ecstasy of light and darkness. They were unaware of themselves and of each other, acting as natural forces in the blackness of great deep space. This period is referred to in myths as The Great Unconsciousness, when nothing but the deities themselves existed as powerful spirits.

At the peak of divine ecstasy came manifestations of the emotions, and entities gained conscience. The Gods awoke and became enlightened and aware. Together, they decided to create. Each contributed their talent, and forth came the stars and the astral bodies, and finally, a planet populated with life in many forms. This was written to be called The Creation.

After creation of Eiradir, life and existence became more complex, and more specific details had to be defined. At that moment, The Gods came to disagreement. Some Gods wanted to write music, while others wanted to hear noise. Some wanted color where others wanted grayness. Some wanted beauty where others wanted grotesque form. This dire time was when the Gods entered a conflict over influence. The battle was heart breaking and destructive, leaving several gods angry and hateful, others damaged and broken. These were the dark days for all, The Gods and the mortals alike. This time was forever known as the Great Celestial Conflict. In the end of the war, so the legend says, The Great Truce was agreed upon, a pact of peace to prevent destruction of all.

The deities agreed to split areas of their influence across the different aspects of mortal life, and divided into two camps based on their agreements, disagreements and affinities. Some Gods were motivated to bless, teach, and guide mortal races in life towards ideals. Others devoted themselves to mischief, temptation, punishment, and retirement of mortal kind.

## Throdin, God of Arts and Crafts

Patron of crafters and artisans, Throdin respects and favors those who make their living with honest work and are not afraid to get their hands dirty.
Almost all dwarvenkind see Throdin as their blessed father, and temples in his name are often erected in their dungeons. However, all craftsmen and artists hold him in high regard.
A quiet god, his blessings are often subtle but long lasting, his curses harsh and unyielding.
It takes much to anger Throdin, but once provoked, his wrath cannot be easily turned away.
Worshippers of Throdin often leave raw materials or their best work to gain his favor. Those who shirk hard work or shortcut their duties will incur his anger.

- Qualities: Stubborn, Hard Working
- Appearances: A stocky man in a workshop apron with hands covered in splinters.

## Priroda, Goddess of Nature, Health, and Fertility

Priroda is popular among all who work the lands or gather the gifts of the sea: peasants, fishermen, farmers, rangers, druids, and all others who live off the nature and respect it.
She is usually prayed to for good harvest, for quick recovery from illness, and for successful childbirth.

Due to her peaceful and caring nature, Priroda is a positive influence on children, and popular deity amongst adults. Her worshippers are widespread in all corners of the world. Priroda is generally described as a giving mother figure, gifts to her followers are believed to be given in generous abundance, and truly loyal followers are said to enjoy years of good health or enough food for a whole village. However, provoking her wrath is extremely unwise, for you would be shaking the very ground beneath your feet.

Worshippers of Priroda leave offerings of anything they can spare. She is a kind and loving goddess, who appreciates the gesture of the offering more than its contents. Her irritation is directed to the greedy and the wasteful, those who have much, but never share it with others, and those who deplete nature without need.

- Qualities: Compassionate, Generous, Selfless, Forgiving
- Appearances: A rotund woman in shape and personality, she may occasionally be seen in the field at sunrise, a young peasant girl with flowing wheat colored hair and a welcoming smile, or other times as a wandering druid with hair the color of dark wood and wearing a green cloak.

## Calypso, Goddess of Magic, Knowledge, and Wisdom

Calypso is seen as a spiritual muse of scholars, prophets, and mages. She motivates the curious and rewards the patient. Her religion is a curious one, because while it promotes freedom of mind and expansion of thought, it is also one of strong indoctrination, almost a dominance of form. It is believed that one must be fully devoted to be truly blessed.

It is believed by her followers, that Calypso was the first to awaken her mind after the Great Unconsciousness, and that she used her magic to awaken the others. It is claimed that creation of the world was her conception, and her design.

Worshippers of Calypso are less widely spread and lack in numbers, but certainly make up in fanaticism and ethical arrogance. Those not initiated in her faith have to prove themselves to be of fitting mind to be accepted into cliques of mages who admire her. More often than not, these cliques end up among the most successful in their studies, which they attribute to her favor.

It is worth noting that those who follow Calypso, claim strong antipathy towards Nephar and a somewhat disdainful look upon Lelei. It is said that Calypso dislikes Nephar's reliance on luck, chance, and instability of mind, being its opposite, while she finds Lelei to be too inconsiderate, flighty, and wasteful, as the drive towards pleasure distracts from the drive to progress and discover.
It is also, however, worth noting, that where as Calypso herself is definitely wise enough not to insult other deities, for she understands the nature of things and the difference between the Gods, not all her followers are always that wise.

Not all followers of Calypso are mages. Libraries are a common place of gathering for all who respect her and seek inspiration - historians, writers, engineers, scholars, druids, alchemists, philosophers.
Places where knowledge is stored are believed to be the focus of Calypso's attention. Be it a lucky breeze that opens a book on the right page, or a prophetic dream that unlocks a secret, the influence of this Goddess is so subtle and easily blended with mortal idea of invention, that sometimes the two are considered one.

Rumors go about speaking of existence of a magical staff, that imposes the will and gifts of Calypso onto its bearer.

- Qualities: Patient, Studious, Inquisitive, Highly Intelligent, Wise
- Appearances: Calypso is not often seen. Like her element, she is highly elusive. Those touched by her staff, however, speak of visions of a finely dressed beautiful woman of aquamarine skin and white hair under a golden robe. Proud, powerful face features, and eyes of blackness, within which stars of the universe shine.

## Lelei, Goddess of Pleasure, Passion, Wine, and Festivities

She comes in many forms, appealing to both men and woman alike, a beautiful nude maiden, a handsome young man, a siren, a satyr, a whisper in the wind. She is playful and light-mooded, making wine seem sweeter, music - more sensual, and people - more beautiful. Lelei is a muse for poets, bards, singers, and wandering minstrels. She smiles upon brewers of wine and makers of sweets.

Her influence is controversial, for she rejoices in seeing people fall in love just as much as she does in seeing them break their vows.

Legends say that Lelei is a vain creature, who lives in a beautiful celestial abode full of crystal mirrors, through which she can see herself and enjoy her beauty. On Eiradir, she is said to enjoy seeing lavish statues of herself, favors all forms of partying, celebration, and merrymaking, specifically those which find release for sexuality. It is said she relishes people whom she can sway to compromise their mortal relationships, because it reminds her of the power her beauty holds, and flatters her.

Hedonistic in her core, Lelei is a temptress - that can be said for certain. Some people take her gifts for blessings, seeing love where others only see lust.

Lelei is widely worshipped. Many traditions form around her.
In a number of human cultures young girls cut off their braids when reaching their first bleeding cycle of maturity as a gift to Lelei. Lelei is said to grant them a small gift of beauty, and their hair grows back fast, longer and stronger, healthier than before. The ritual entails placing one's cut off hair in a cloth, tying a ribbon into it, wrapping the cloth, and letting it flow downriver, or placing it under a statue, painting, or icon of Lelei, or bringing it to Lelei's altar itself, depending on culture.

In the elven culture, an elfess who reaches maturity, sometimes completely shaves her hair, and not just the braid, becoming completely bald for a time. She says her prayer to Lelei, but she does not stay indoors until her hair grows out, instead she spends the next month highly social and partaking in festivities, covering her hairless head with a white headdress, and it has been known to see these young elfesses regain their beautiful hair in mere weeks, in just the perfect way they always desired.

Legends of all cultures agree on one thing: offending Lelei is extremely unwise. She is notoriously vengeful.

- Qualities: Seductive, Playful, Jealous, Pleasure Driven, Vengeful
- Appearances: To men Lelei appears as a young nude woman of extreme beauty and stunning shape, leaving just enough room for wild imagination. It is said that one who likes perfection, will find her perfect, while one who likes little imperfections, will see just enough of them in her form. To women, Lelei appears as a handsome young man of robust complection, or at other times as a playful mentor, guiding them to explore their pleasure senses. At times, she can be seen bathing in secluded waterfalls.

## Nephar, Goddess of Chaos, Luck, and Random Chance

Nephar is an entity that defies law, structure and organization.

Originally a goddess by legend, this deity has lost her personality and identification when she went insane during the celestial conflict known as the Great Conflict. Said to be, perhaps, the second most grievous wound to the celestial pantheon, next to awakening of Koshmar, the loss of Nephar's mind introduced a shifting change into the structure of nature.

Today Nephar is known as both male and female, and the topic is a debate for many scholars. Out of fear of disrespect, some followers address Nephar as It, rather than He or She, while others in turn consider that disrespectful in itself, and prefer to choose a gender. Some people pray to Nephar as a last resort, her name can be heard uttered in prisons, sometimes even on the chopping block. Though, one must be truly living on the edge of desperation to say their last prayer to this entity. There are a number of better candidates to pray to in the dire hour, and yet, some mysterious souls live within a strange code and hold Nephar as their patron.

Among general population, Nephar is equally feared, pitied, and respected. It lives unpredictably, and favors all things unpredictable. Acts upon whims and does what it wants. Now and then it favors risk-takers, gamblers and those with damaged minds. One should be careful when praying to it.

- Qualities: Chaotic, Treacherous, Playful, Insane
- Appearances: None know the true face of Nephar. Sometimes she appears as a woman, stunningly beautiful in body, but lacking a face and screaming in agony. Other times, a laughing and crying man in a mask of ever changing colors. Some report seeing a gambler in various tavern, so lucky and dashing that Nephar's own soul can be seen in the gleam of their eye.

## Heraphis, God of Travel, Trade, and Commerce

All major markets and crossroads have a shrine to Heraphis.
Docks and ships are often dedicated to his name.
Be they buying or selling, any who live off a good deal pray to him.

All adventurers who plan a long journey visit his shrine for good luck.
Exploring expeditions and trade caravans speak of his name in their travels across mountains and deserts
Be it in the windy tundra, thick taiga, or saultry steppe, all who walk the roads pray to him.

When you hear the voice beckoning you to explore and discover, you hear the voice of Heraphis.

- Qualities: Freedom Loving, Free-Spirited, Curious, Witty, Daring
- Appearances: Those who journey long distances be it by land or sea often bring with them tales of a man clad in wide brimmed green hat standing just over the horizon. Others see in the skies a cloud which seems as a winged man in a long and wide golden robe and a wreath of swallow feathers, pointing the way.

## Vorrigan, God of Thieves and Shadows

Protector of rogues, assassins, prostitutes and beggars.
Rumors are all that exists of Vorrigan. A trickster with a dark sense of humor and a likely patron of the unfortunate people who live in the streets.
Vorrigan respects risk takers who defy authority and those who steal from the rich to give to the poor. Thief clans are rumored to have established organized worship. Generally a jovial character, in his darker moods he can be dangerous. It is said that now and then Vorrigan manifests among mortals to assist in thieving jobs with those he favors, or on rare occasion, helps render those who come close to being caught invisible.

Gossip speaks of a legendary thief Graylock, who knows no rivals in manipulating locks. He may tell you of a secret shrine to the Shadow God, if you manage to find him and prove worthy of the trust of the Thieves' Guild. There, one may find such dark legends of burglary as pickpocket Sly, road bandit Black Fox, or the notorious Red Mist.
One who preys on the uncareful in the night streets is wise to pay tithe to Vorrigan, for his favor can be a trump card between life and death.

Not all thieves hold Vorrigan's favor. The word on the street is that it requires following a certain code of honor, but only the initiated know what it is.

- Qualities: Devious, Secretive, Inquisitive, Mischievous
- Appearances: Sometimes he comes in the guise of a beggar on the street with eyes gray as a raincloud, other times a pudgy merchant with a rich emerald ring on his finger, more rarely a dark haired man with a gray stubble, dressed in a gray hooded cloak and smoking a long pipe.

## Sakira, Goddess of Hunt and Battle

The sound of her horn invigorates the hearts of warriors, and her blessings are prayed for by those going to battle, expecting to die for their cause.
She favors courage, honor, and valor. Fear a warrior who puts their faith in Sakira, for their arrow will fly true, their sword will not chip and their shield will not shatter. Hunters say their prayers to this proud and beautiful Goddess, to bless the hunt with success. Some say they have heard her horn far ahead in the forest, and had seen a mighty and graceful white hind. This is considered a good omen.

Worshippers of Sakira often leave her offerings of captured game or battle tested weapons as a sign of their respect to her. Those who flee from battle or fight without honor: cowards, traitors, knaves, merit her dislike. Also, those who kill her creatures without need, do so at their own peril.

- Qualities: Brave, Passionate, Proud, Honorable
- Appearances: Forever young and tan, she wears her hair in a series of braids, clad in silver armor or leathers.

## Methos, God of Death

Beyond the boundaries of the living world, beneath the ethereal veil of death, stands a Gate of Five Thousand Truths, guarding the entrance to the Underworld, where no living mortal can enter.

The face of Methos, The Gatekeeper of The Underworld, is the first face one sees when one dies. The ferryman, the grim reaper, the shrouded figure with a scythe, the two faced swordsman on a pale horse, all these may describe Methos. Methos judges the souls of those who have reached their end and delivers them to their final destination. Otherwise neutral, his only motivation is to ensure that cycles of life and death remain intact.
He is feared by those who see death as a punishment and welcomed by those who see death as a relief.

According to myth, Methos existed since the time of The Great Unconsciousness, but awakened only when The Creation began, because that was also the time to destroy. As soon as the cycle of life and death came to be, there was a place for him to complete it. Methos stayed unwavering and neutral during The Great Celestial Conflict and was among the wiser few who first proposed The Great Truce. His wisdom and knowledge, surprisingly, stood up a number of other deities, who were still concerned with their pride.

Methos is not considered an evil god, for he does not actively seek destruction. However, the nature of death makes him a dark one none the less, for his realm is to destroy, and his rules are unbending. Methos upholds the natural cycle and reserves his wrath for those who defy it. Mortals attempting to laugh at death or escape it by some mystical means or others, displease him. From time to time, things go amok in the cycle, and strange phenomena occur, such as the various undead. It is on such occasions that Methos sends down his personal Revenants, called the Black Paladins to combat this problem. Existing on loaned lifespan, the Revenants are souls loyal to Methos, and serve to seek out the escaped violators and bring them to order.

- Qualities: Calm, Cynical, Ascetic
- Appearances: A tall thin figure in a black hooded robe, with face of obsidian black, ivory white, or sometimes skeletal. Sometimes seen as a reaper carrying a scythe, or as a lonely ferryman holding a black iron car standing by the dark water, with a boat awaiting.

## Koshmar, God of Hatred, Terror, and Nightmares

Behold and beware, as the all seeing seven eyes of Koshmar seek every way to lurk into your soul. This dark and cruel God has many names: The Dread Lord, The Devil God, The Unwanted. A hissing whisper, an agonizing scream in the night, an unresting evil which comes on wings of darkness, Koshmar and "nightmare" are synonymous in the mind of most people.

Koshmar's history is a murky tale which few dare to inscribe for the fear of the wrath of the other Gods. However, what is known and has passed on, was enough to understand the tragedy which came with Celestial Conflict.

According to myth, The Dread Lord did not wake together with the other Gods from the Great Unconsciousness. Koshmar was awakened, or perhaps created, much later, during the culmination of the Great Celestial War, when all the Gods were in disagreement and strife over how the universe they created should work and run, in particular the argument over the fate of the world of Eiradir. This makes Koshmar the youngest of the Gods, but the extent of his power more than rivals theirs. Unlike mortal thoughts and emotions, thoughts and emotions of Gods carry potency of divine creation, which contain power to manifest into tangible being. Thus, when the deities expressed their negative emotion and began fighting, this negative energy accumulated and tainted parts of their own spirits, which were able to separate from the original Gods and combine into one, new, separate entity, consisting entirely of negative emotions of conflict and hatred.

During the Conflict, the Gods showed their worst, darkest sides. Things like honor and humility were set aside. Sakira was ironically the one to blame most. Sakira's strength, vigor, and courage became cruelty, violence, and pride. Those emotions became primary in Koshmar's form. Lelei's power of temptation and persuasion, manifested in those of Koshmar. Because of Throdin's stubbornness and determination, Koshmar gained the supply of patience to execute his will. Priroda's destructive side granted Koshmar power over shape of matter and taught him destruction. Vorrigan's tendency to bend the rules, manifested in Koshmar's tendency to outright break them. Heraphis was perhaps one of the least to blame, but his trend to silently do his own bidding ignoring others, became that of Koshmar as well. Methos was obsessed with the cycle of life and death, and his intimate connection to death, undeniably clad Koshmar in black colors. Nephar, who could see Fate, granted Koshmar ability to see it and twist it, when she tried to manipulate other Gods using visions. The final blow was delivered by Calypso's tragic attempt to dominate Nephar's mind. The negative energies became intense, driving Nephar insane and plunging her spirit into Chaos. This bit of madness became the final straw, which woke Koshmar's mind.

Koshmar rose, with combined dark powers of all the Gods, and quickly realized his strengths, when he felt fear coming out of them and feeding himself.
It was a long and fierce battle, which nearly led to the end of all things, until a compromise was reached, and Koshmar was restrained with combined powers of the nine. At that point, The Great Truce was declared, but Koshmar was not destroyed, for his presence in mortal minds was too strong now, forever binding him to existence. However, weakened, Koshmar retreated into his infernal domain, which he created for himself, and from there, he did his work of trying to lean the world of mortals towards darkness.

Koshmar's first attempts to change the world to fit his vision consisted of unleashing legions of demons onto Eiradir. Those attempts were met with brave spirit and strong resistance of Eiradir's mortals, and while they generated plenty of fear for Koshmar to feed, they did not last long, and did not provide him a stable footing in the world. Brave heroes sent the demons back to Inferno and destroyed their shells over and over again. Just like the Gods, their mortal followers united. Knights, mages, rangers, and thieves, all set their differences aside and fought side by side.

Time passed, and Koshmar changed his strategy. He deduced that to keep a firm grasp on the mortal world, he needs to establish worship, and behave more as a God than as a Devil. He looked at what the other Gods did with the beings they created, and noted one difference. The Nine Gods gifted their children with free will. This, of course, meant that the children may disobey and not follow the Gods, or even forget them, but such was the price. Koshmar considered this his only option. He had to create a race of sentient beings who have their own will, unlike the demons, which would insure their survival and acceptance in the mortal world. So, Koshmar created the orcs.

The result was a two edged sword. On one end, it was a stellar success, for the orcs were highly disciplined and organized. Their structure rivaled that of dwarves, and their spirituality was not far behind that of elves. The orcs survived and established themselves in the world.
On the other end, only a fraction of orcs continued to follow the faith of their Dark Father, while the rest spread around the planet and diversed in their beliefs. Once more, Koshmar was prevented from a full success. Such was the meaning of free will.

When Throdin and Methos forged the Gate of Five Thousand Truths to separate Eiradir and the Underworld, Koshmar was able to corrupt it unbeknownst to others, allowing him to use the Gate to draw from the Well of Souls and create his own Revenants, the Crimson Reapers, revived souls of his devout followers who died in his name in their previous lives. Truly sick, twisted, and dark beings, the Reapers terrorize the lands of Eiradir to this day.

Koshmar's best move, however, was learning how to create a channel through which he could directly manifest his powers in the realm of Eiradir, despite his formal agreement to not interfere with affairs of mortals. Because of this shrewd betrayal of the Truce, every newly born mortal holds a bit of Koshmar's taint, leaving a door open for the Dread Lord's dark influence. The influence we call nightmares, fears, and cowardice.

- Qualities: Cruel and Sadistic, Manipulative and Tempting, Ruthless and Unforgiving
- Appearances: No one was able to describe Koshmar's exact appearance, for seeing his true form instantly drove people insane. It is said that he looks into mortal souls and manifests to each person a combination of things they fear most, from a swarm of insects to a creature consisting entirely of bleeding bodies of one's relatives. Many writings, however, refer to "Seven All-Seeing-Eyes", and a crown the color of obsidian.
