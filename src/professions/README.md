# Vocations and Professions

Eiradir is an open skill sandbox game. Given enough time and effort anyone can master any skill, though different characters will excel differently in various trades, because of differences in their attributes. Below is a list of the various professions available in Eiradir and what each entails. Take note that Eiradir is a classless game, thus the professions below are thematic recommendations and merely that, to give you an idea of what possible careers you may choose. Multiple professions may be combined in any order.

## Farmer

Includes gathering of wild grown resources such as berries and fruits, planting and harvesting of farm fruits, vegetables, and grains, bee tending and honey gathering, and the preparation of soil for planting. A wise farmer will always take heed of the present season as well as the climate of his location in order to maximize his yield. Farmers are pioneers of settlements and are the first to claim new outreach for civilizations, because they can turn wild grass into farmland with the use of trusty tools: plows, rakes, and shovels. They must maintain their farms, or else the nature's weeds and wild roots will take the territory back.

## Herbalist

Involves identifying useful herbs, gathering of herbs, storage and preparation of herbs for alchemic or medicinal use. The profession of a true explorer and woodsman, not many will follow it, for unlike others, it requires great patience and knowledge of seasons, flora and fauna. A skilled and wise herbalist will always be in high demand, as only they can find the rarest of plants.

## Lumberjack

Highly physically demanding profession including abilities to identify trees which produce certain types of wood, as well as the logging and re-planting of those trees. Woodsmanship involves the initial harvesting and transport of trees for ultimate processing into wood products. A male predominant trade, it requires good strength and endurance to fatigue. A skilled lumberjack will be more efficient, falling trees faster and sawing them into better quality logs. Lumberjacks are the pillar and foundation of civilized cultures, and are always in demand.

## Miner

A miner's profession involves identification of ore, gems, natural minerals, quartz sand, or coal within a rock surface, and the collection of said resources. A miner's friends are pickaxe and shovel. The mining industry is as old as civilization itself. Few can compete with the dwarven miners of Mount Hammerfist, but truly skilled miners can find many riches within the mountanous caverns: iron, silver, copper, gold, teranium, malphite, and many kinds of gemstones. Miners work in close conjunction with smiths, and many miners are smiths themselves.

## Fisher

Pull, pull! Here comes a big one! A profession of great skill and great braggery, its art and craft overlooked by many. Anyone can fish, but not everyone can fish well. Catching fresh fish is profitable, as it can be prepared into many tasty dishes, and cooks will pay the coin to have the rarest. A true professional fisher takes account of everything: season, time of day, weather, wind, and location. Most importantly, the habits of the fish they stalk. Many different kinds inhabit the waters of rivers and seas. Their migration and feeding patterns don't escape the eye and mind of a true fishing master.

## Trader

Elusive and always up in the air, surfing the wave of luck and social climates, the profession of an economist is a dangerous one. At first sight, it is simply buying and selling, but in reality it is much more. In business, everyone is for themselves, and man to man is a wolf. To make a deal, to turn a wolf into a friend, and to have partners come back for more - that is the secret of a trader. A merchant detects fluctuation of currency and watches the market to maximize profit. And of course, ability to conduct proper service is equally important. Some traders drive a hard bargain and maintain their status quo, others can bend and be flexible, because they remember: the customer is always right, and the art of good business is being a good middle man.

## Carpenter

Carpentry involves preparing wood for use via a carpentry table and the means to produce and repair tools, weapons, shields, and household items made out of various types of wood through the use of learned designs. Carpenters rely on lumberjacks, and are experts at which types of wood can be used for which specific crafts. Their tools of trade are saws, carving tools, and planes. Along with the smiths, these artisans are among the wealthiest professionals in the world. Their path takes a long time to master, but it pays off greatly in the end.

## Blacksmith

A blacksmith forges ore into ingots via the use of a coal burning forge and the means to produce tools, weapons, armor, shields, and household items out of various types of ingots through the use of learned designs. Blacksmiths are truly the drivers of progress: their constant striving to perfection changes the civilization in the new ways to evolve lifestyle and warfare. A warrior's good friend, a skilled blacksmith can lighten the armor and sharpen the blade, mend a dent, and rebuild the most broken of steel. Those who choose the path of the forge are among the economical elite, for their patience is rewarded with truly handsome gold. It's hard work, and it's hard money.

## Cook

Everyone has to eat, and cooked food is better than raw. But there is a difference between simply eating and eating well. And a good cook always comes in handy in any party. Cooks prepare various dishes through the use of gathered ingredients and learned recipes. In their arsenal are an oven and an open fire; their tools of trade are spoons, ladles, and knives; their specialties are pork, fish, beef, fowl, venison, lamb, fruits and vegetables. Knowledge of tastes and spices, the proper herbs and seasonings - compose the art of bringing out the most taste out of any meat. Most people can cook something up quick and easy, but professional cooks can live up to serve kings.

## Baker

A baker prepares various baked goods through the use of gathered ingredients and learned recipes. Servant of the baking oven, armed with a rolling pin, a baker can spoil you with a tasty dessert or offer warm loaf of bread to sate your hungry belly. As the smoke comes out of the pipe, the smell of fresh goods being baked draws many hungry noses from around a mile away.

## Tailor

A paragon of fashion, a tailor plays one of the main roles in culture. The profession includes gathering wool, flax, silk, cotton, fur and uncured leather from various resources, spinning thread out of fibers, weaving cloth though the use of a loom, curing fur and leather, and sewing clothing, armor and various household items from these resources through the use of learned designs. Tailoring is a useful skill for keeping warm and dressing sharp, whatever the occasion. As a result, tailors are in high demand and tend to get quite wealthy. The job is not for the clumsy fingers, so dexterity is a definite requirement, else you shall find a needle in a boot. Hopefully, not your own.

## Constructor

Where people go, so go their dwellings. Builders are the very fundamental profession of the body of civilization. Even the most primitive tribal cultures understand the concept of construction. More advanced civilizations employ proper approaches, and their builders are also engineers and architects. This profession is highly diverse and overwhelmingly versatile. Using wood, metal, glass, and stone, provided by lumberjacks, glassblowers, and miners, constructors erect houses, establishments, and castles to define their presense upon the world. In a way, one could say that a culture is shaped by its architects.

## Healer

A healer identifies ailments and injuries, to produce bandages from clean cloth, herbal potions, salves and poultices through the use gathered herbs and learned recipes, and in some cases the invocation of mystical healing spells. There are many different kinds of healers in the world, some are alchemists, others are herbalists, others are mages, and yet others are a mix of all those. Healers usually lead a roaming liftstyle, though some settle in monasteries or open clinics. No doubt that a healer spends a lot of time travelling to gather and collect ingredients for preparation of their remedies. A good healer is a kind humanitarian, always studying the living condition.

## Alchemist

Alchemy includes the ability to identify reagents within herbs and animal matters and mix various components into useful potions though the use of learned recipes. An alchemist goes beyond the healing realm and delves into more complex secrets of nature. Three primary products of alchemy are elixirs, oils, and bombs. Alchemists often lead a secluded way of life, and their secrets are strictly kept. Some are known to live and study in communities, scattered around the map. An alchemist is to be feared and revered, one never knows what may be up their sleeve.

## Glassblower

A house without glass windows is hard to imagine nowadays. Through a window of glass, one can see, yet keep the cold away. Wine could last a few days in a leather wine skin, but it will last years in a glass bottle. The ultimate window material and a carrying vessel stunned the world, when the secret of glass production was discovered upon the world. It was the glassblowers who brought out this discovery. A glassblower makes use of quartz sand, ash, and lime, with aim to purify the ingredients, melt and blow glass into various household items and bottles through the use of learned recipes. Glassblowers work in conjunction with miners, who go out in the desert to search for quartz sand. In turn, glassblowers sell their goods in bulk to builders, alchemists, brewers, and healers.

## Jeweler

A jeweler identifies useable precious gems and determines their value, with purpose to cut raw gems into gemstones and produce rings, necklaces, bracelets, earrings, and crowns through the use of learned designs. This, at the first glance small profession was made necessary by the need of people to demonstrate power and wealth, and later on, protect themselves from magic power or, on the contrary, enhance its effects. Today jewelers are among the wealthiest specialists in the world, competing with blacksmiths, builders, tailors, and carpenters.

## Brewer

They say that brewing is more an inborn gift than a learned profession. A skilled brewer must have a keen nose and a sharp instinct. Their trade involves collecting grains, berries, herbs, and sometimes other secret ingredients, and using yeast to ferment and distill alcohol, producing refreshing and entertaining beverages. Brewers are everyone's friends, their business welcome at celebrations and parties.

## Musician

A musician is a servant of their muse, a traveler vagabond, or a settled courtier. They compose melodies and songs, and play the many instruments for the enjoyment, entertainment, or education of people. Music, along with fashion and architecture, is one of the defining spheres of civilian culture. Many find a musician attractive, for it is a rare gift which can put a smile on your face, or bring a tear to your eye.
