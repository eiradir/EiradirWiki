# EiradirWiki

Public wiki for Eiradir lore and things that are publically known or easily researchable ingame - famous characters, legends, common items and how to get them, etc. 

Based on vuepress.

## Contributing

We accept Merge Requests in this repository.

- Information must be generally relevant to the player base
  - As cool as your character is, they're probably not a legend (yet) 
  - Long-lasting and established guilds as well as people of interest are fine, though
- Information must be publicly known or easily deductible
  - For example, every knows (or can easily find out) the rumors about who Zamissiel slept with - however, his inner motive of sleeping with that person is obviously unknown to the general public
  - Similarly, everyone would be able to know that nails exist, and that they are crafted through blacksmithing - so that would be fine (and beneficial to new players) to be posted on the wiki

### Getting Started

- You will need a GitLab account
- At the top right, click "Fork" to create a copy of this repository (if you are part of the Eiradir team, this is not necessary, as you can create branches directly on the main repo)
- Use a Git Client to clone your fork, or use GitLab's Web IDE to edit in your browser
- Edit or create new Markdown files in the "src" directory, edit or add new sidebar links in the "src/.vuepress/config.js" file
- Use your Git Client or the Web IDE to commit and push your changes
- To submit your changes for review, head to the [Merge Requests](https://gitlab.com/eiradir/EiradirWiki/-/merge_requests) section of the main repository and press "New merge request"
- Select your fork and the branch you committed changes to on the left as source branch, and `eiradir/EiradirWiki` with branch `master` as target branch
- Click "Compare branches and continue", and shortly describe your changes, then submit

#### Previewing your Changes (Method A)

- If you'd like to see a preview of your changes, use a Markdown compatible editor (the Web IDE has a `Preview Markdown` tab)

#### Previewing your Changes (Method B)

- If you'd like to see a preview inside the actual Eiradir Wiki, you can't use the Web IDE and must clone the repository to your local drive
- Install [NodeJS](https://nodejs.org/en/) LTS 
- In the cloned directory, open a terminal or Powershell (control-shift-right-click to see the menu entry on Windows)
- In the terminal, type `npm install` and then `npm run serve` once it is complete
- Open your browser at http://localhost:8080 where you will get a live view of your changes inside the actual wiki styling
